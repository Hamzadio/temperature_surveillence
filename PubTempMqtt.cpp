#include <string>
#include <iostream>
#include <stdio.h>
#include <mosquitto.h>
#include <SenseHat.h>

using namespace std;

int main() {
  int rc;
	struct mosquitto * mosq;

	mosquitto_lib_init();

	mosq = mosquitto_new("publisher-test", true, NULL);
	mosquitto_username_pw_set(mosq, "AlbainCottancin", "ciel1");

	rc = mosquitto_connect(mosq, "10.0.134.53", 1883, 60);
	if(rc != 0)
  {
		printf("Client could not connect to broker! Error Code: %d\n", rc);
		mosquitto_destroy(mosq);
		return -1;
	}


	printf("We are now connected to the broker!\n");
 
    SenseHat carte;
    
    cout << "Application Temp Running" << endl;

    carte.Effacer();
    carte << setcouleur(carte.ConvertirRGB565(64,84,0));

    while(1)
    {
	
      cout << "temp= " << carte.ObtenirTemperature() << endl;
    	
 
      float Temperature = carte.ObtenirTemperature();
      string TempearatureString = to_string(Temperature);
 
      int Lenghtmessage = TempearatureString.size();
      const char* Messagechar = TempearatureString.c_str();

	    mosquitto_publish(mosq, NULL, "test/topic", Lenghtmessage, Messagechar, 0, false);
    }
    
  
 
  
	mosquitto_disconnect(mosq);
	mosquitto_destroy(mosq);

	mosquitto_lib_cleanup();
    
    
    
    
    return 0;
}